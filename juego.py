from mazo import Mazo
from jugador import Jugador
from pila import Pila

class Juego:
    def __init__(self):
        self.mazo = Mazo()
        self.pila = Pila()

    def repartir_cartas(self, jugadores):
        self.mazo.barajar()
        cartas_por_jugador = 5
        cartas_repartidas = 0

        while cartas_repartidas < cartas_por_jugador * len(jugadores):
            carta = self.mazo.cartas.pop(0)
            jugador_actual = jugadores[cartas_repartidas % len(jugadores)]
            jugador_actual.cartas.append(carta)
            cartas_repartidas += 1

    def tomar_y_descartar_carta(self, jugador):
        print(f"{jugador.nombre}, elige una acción:")
        print("1. Tomar una carta del mazo.")
        print("2. Tomar una carta de la pila.")
        print("3. Ver la pila.")
        print("4. Ver mis cartas.")

        while True:
            opcion = input("Selecciona una opción: ")
            if opcion == "1":
                if self.mazo.cartas:
                    carta = self.mazo.cartas.pop(0)
                    jugador.cartas.append(carta)
                    print(f"{jugador.nombre} tomó una carta del mazo.")
                    if len(jugador.cartas) > 5:
                        print("Tienes más de 5 cartas en tu mano. Debes descartar una.")
                        self.mostrar_cartas(jugador)
                        self.descartar_carta(jugador)
                    break
                else:
                    print("El mazo está vacío.")
            elif opcion == "2":
                carta = self.pila.tomar_carta()
                if carta:
                    jugador.cartas.append(carta)
                    print(f"{jugador.nombre} tomó una carta de la pila.")
                    if len(jugador.cartas) > 5:
                        print("Tienes más de 5 cartas en tu mano. Debes descartar una.")
                        self.mostrar_cartas(jugador)
                        self.descartar_carta(jugador)
                    break
                else:
                    print("La pila está vacía.")
            elif opcion == "3":
                if self.pila.cartas:
                    print(self.pila.cartas[-1])
                else:
                    print("La pila está vacía.")
            elif opcion == "4":
                self.mostrar_cartas(jugador)
            else:
                print("Opción inválida. Por favor, selecciona 1, 2, 3 o 4.")

    def mostrar_cartas(self, jugador):
        print(f"Cartas de {jugador.nombre}:")
        for i, carta in enumerate(jugador.cartas, 1):
            print(f"{i}. {carta}")

    def descartar_carta(self, jugador):
        while True:
            opcion_descartar = input("Elige una carta para descartar (1-6): ")
            if opcion_descartar.isdigit() and 1 <= int(opcion_descartar) <= 6:
                carta_descartar = jugador.cartas.pop(int(opcion_descartar) - 1)
                self.pila.agregar_carta(carta_descartar)
                print(f"{jugador.nombre} descartó la carta: {carta_descartar}")
                break
            else:
                print("Opción inválida. Por favor, elige un número entre 1 y 6.")

    def obtener_jugadores(self):
        while True:
            number_of_players = input("¿Cuantas personas juegan? (2-4): ")
            if number_of_players.isdigit():
                number_of_players = int(number_of_players)
                if 2 <= number_of_players <= 4:
                    jugadores = [Jugador(f"Jugador {i + 1}") for i in range(number_of_players)]
                    return jugadores
            print("Por favor, ingresa un número válido entre 2 y 4.")


    def verficar_ganador(self, jugador):
        valores = {}

        for carta in jugador.cartas:
            valor = carta.valor
            valores[valor] = valores.get(valor, 0) + 1

        if len(valores) >= 2 and 3 in valores.values() and 2 in valores.values():
            print(f"Felicidades, {jugador.nombre} ha ganado el juego!")
            return True

        return False

    def iniciar(self):
        jugadores = self.obtener_jugadores()
        self.repartir_cartas(jugadores)

        is_active_game = True
        current_player = 0

        while is_active_game:
            print(f"Turno del jugador {current_player + 1}:")
            self.tomar_y_descartar_carta(jugadores[current_player])

            if self.verficar_ganador(jugadores[current_player]):
                is_active_game = False
                break

            current_player += 1

            if current_player == len(jugadores):
                current_player = 0
