from juego import Juego

if __name__ == "__main__":
    continuar = True
    juego = Juego()

    while continuar:
        print("¿Desea jugar tres y dos?")
        print("Jugar [j]")
        print("Salir [q]")
        opcion = input().lower()

        if opcion == 'j':
            juego.iniciar()
        elif opcion == 'q':
            print("¡Fin del juego!")
            continuar = False
        else:
            print("Opción no válida. Por favor, ingresa 'j' para jugar o 'q' para salir.")