import random
from carta import Carta

class Mazo:
    def __init__(self):
        self.cartas = [Carta(valor, palo) for palo in ['Corazones', 'Diamantes', 'Tréboles', 'Picas']
                       for valor in ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A']]

    def barajar(self):
        random.shuffle(self.cartas)

    def imprimir_mazo(self):
        for carta in self.cartas:
            print(carta)

    def imprimir_cuenta_del_mazo(self):
            print(len(self.cartas))